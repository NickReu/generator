package sphere

func Neighbors(i int) []int {
	switch i {
	case 0:
		return []int{4, 1, 5}
	case 1:
		return []int{0, 2, 7}
	case 2:
		return []int{1, 3, 9}
	case 3:
		return []int{2, 4, 11}
	case 4:
		return []int{3, 0, 13}
	case 5:
		return []int{14, 6, 0}
	case 6:
		return []int{5, 7, 15}
	case 7:
		return []int{6, 8, 1}
	case 8:
		return []int{7, 9, 16}
	case 9:
		return []int{8, 10, 2}
	case 10:
		return []int{9, 11, 17}
	case 11:
		return []int{10, 12, 3}
	case 12:
		return []int{11, 13, 18}
	case 13:
		return []int{12, 14, 4}
	case 14:
		return []int{13, 5, 19}
	case 15:
		return []int{19, 16, 6}
	case 16:
		return []int{15, 17, 8}
	case 17:
		return []int{16, 18, 10}
	case 18:
		return []int{17, 19, 12}
	case 19:
		return []int{18, 15, 14}
	}
	return []int{}
}
