package nation

import "gitlab.com/NickReu/generator/sphere"

type Nation struct {
	Name  string
	Realm map[int]bool
}

func (n Nation) GetRealm() []int {
	m := n.Realm
	keys := make([]int, len(m))
	i := 0
	for k := range m {
		keys[i] = k
		i++
	}
	return keys
}

func (n Nation) GetNeighborCoords() map[int]bool {
	m := n.Realm
	neighbors := make(map[int]bool, 0)
	for k := range m {
		for _, nb := range sphere.Neighbors(k) {
			if !n.Realm[nb] {
				neighbors[nb] = true
			}
		}
	}
	return neighbors
}

func (n Nation) GetNeighborNations(others map[int]*Nation) map[*Nation]bool {
	nns := make(map[*Nation]bool, 0)
	for nb := range n.GetNeighborCoords() {
		nn := others[nb]
		if nn != nil {
			nns[nn] = true
		}
	}
	return nns
}

func (n Nation) String() string {
	return n.Name
}
