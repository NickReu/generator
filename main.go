package main

import (
	"fmt"

	"gitlab.com/NickReu/generator/world"
)

func main() {
	world.NewWorld()
	fmt.Println()
}
