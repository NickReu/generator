package world

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/NickReu/generator/nation"
	"gitlab.com/NickReu/generator/sphere"
)

type Ico struct {
	political map[int]*nation.Nation
	ocean     map[int]bool
}

func NewIco() Ico {
	return Ico{make(map[int]*nation.Nation, 20), make(map[int]bool, 20)}
}

type World struct {
	ns    []nation.Nation
	globe Ico
}

func NewWorld() {
	rand.Seed(time.Now().UnixNano())

	w := World{[]nation.Nation{}, NewIco()}
	w.AddNation()
	w.AddNation()
	w.AddNation()
	w.AddNation()
	w.AddNation()

	for _, n := range w.ns {

		nns := n.GetNeighborNations(w.globe.political)
		nns_slice := make([]*nation.Nation, len(nns))
		i := 0
		for nn := range nns {
			nns_slice[i] = nn
			i++
		}

		fmt.Printf("%v, neighboring %v\n", n, nns_slice)
	}
}

func filterTargets(targets []int, others []nation.Nation, ocean map[int]bool) []int {
	filtered := []int{}
	for _, t := range targets {
		occupied := false
		if ocean[t] {
			continue
		}
		for _, n := range others {
			if n.Realm[t] {
				occupied = true
				break
			}
		}
		if !occupied {
			filtered = append(filtered, t)
		}
	}

	return filtered
}

func nationName() string {
	const uc_letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	const lc_vowels = "aeiouy"
	return string(uc_letters[rand.Intn(len(uc_letters))]) + string(lc_vowels[rand.Intn(len(lc_vowels))]) + "landia"
}

func (w *World) AddNation() {
	seed := rand.Intn(20)
	realm := make(map[int]bool)
	realm[seed] = true
	n := nation.Nation{Name: nationName(), Realm: realm}
	for i := rand.Intn(5) + 3; i > 0; i-- {
		expandFrom := n.GetRealm()[rand.Intn(len(n.Realm))]
		targets := sphere.Neighbors(expandFrom)
		targets = filterTargets(targets, w.ns, w.globe.ocean)
		if len(targets) == 0 {
			continue
		}
		expansion := targets[rand.Intn(len(targets))]
		n.Realm[expansion] = true
	}
	for _, c := range n.GetRealm() {
		w.globe.political[c] = &n
	}

	w.ns = append(w.ns, n)
}
